// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

var API;
//var initialized = false;
var sketchfab = SAGE2_App.extend( {
	
	init: function(data) {
		function do_zoom(scale) {
			this.zoom = scale;
		}

		function createAnnotationElement(){
			var canv = document.createElement('canvas');
			canv.id     = "aCanvas";
			canv.height = 800;
			canv.width = 1400;
			canv.style.position = "fixed";
			canv.style.top = "10px";
			canv.style.left = "10px";
			t.element.parentElement.appendChild(canv);
			t.ctx = canv.getContext("2d");
			t.canvas = canv;
			t.ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
			t.ctx.fillRect(0,0, t.canvas.width, t.canvas.height);
			// hidden by default - press H to show
			t.canvas.style.visibility = "hidden";
		}

		var t = this;
		// call super-class 'init'
		t.SAGE2Init("iframe", data);
		t.maxFPS = 30.0;
		t.resizeEvents = "onfinish";

		// application specific 'init'
		t.element.frameborder = 0;
		t.zoom = 1.0
		t.dragging = false;
		t.moving = false;
		
		t.element.src = '';
		t.element.id = 'fab-api-frame-'+t.id;
		t.element.style.border = "none";
		t.element.style.background = "#fff";
		var version = '1.0.0';

		var urlid = 'a9b569b66ea74e709420718a7dc567e4';
		
		mid=0;
		t.models = [];

		for (var i = 0 ; i < 10 ; i++){
			t.models[i]={};
		}
		
		// position and target is reset value for specific models
		// speed is proper camera speed acording to model size
		t.models[mid].position = [-0.006050028676984454, -2.550986263664178, 0.9575756239030855];
		t.models[mid].target = [-0.006050028676986674, -0.005960464477539063, 0.128709961385727];
		t.models[mid].speed = 1;
		t.models[mid++].id	= "83bea47f761247d1ae4da2f3d27ddbf6"; // 0 amfora

		t.models[mid].position = [0.04179767953871899, -7.241636441345519, 2.5212360923988597];
		t.models[mid].target = [0.041797679538726706, 0.016759001922608263, 0.1628404498338698];
		t.models[mid].speed = 1;	
		t.models[mid++].id	= "a9b569b66ea74e709420718a7dc567e4"; // 1 nadoba

		t.models[mid].position = [4.307429597262609, 2.1044158215340807, 0.8250400708346671];
		t.models[mid].target = [-0.005731319686174352, 0.035867161958813565, 0.1193401307092552];
		t.models[mid].speed = 1;
		t.models[mid++].id	= "36461fbf2077461698da097a9eee33c9"; // 2 gombik

		t.models[mid].position = [0.5649977657434413, -86.46386277638325, 29.296037720128474];
		t.models[mid].target = [0.5649977657431933, -0.30205754236650506, 1.3003701366128695];
		t.models[mid].speed = 1;		
		t.models[mid++].id	= "dda3dd0fba724866bcaff45883fcbf17"; // 3 langweil 08 - fixed

		t.models[mid].position = [-0.48705577850341797, -50.76106966261674, 21.155794155973766];
		t.models[mid].target = [-0.48705577850341797, 3.535447505950928, 3.513786291057972];
		t.models[mid].speed = 1;		
		t.models[mid++].id	= "0ce420aafaa64afe83a37725d3736df4"; // 4 langweil 09 - fixed

		t.models[mid].position = [-23.289147787987094, 23.165699430521702, 18.65555093069672];
		t.models[mid].target = [-1.3883160935304204, 1.0975425109317634, 1.094358652830124];
		t.models[mid].speed = 20;		
		t.models[mid++].id	= "22674a58513d4be5a19094813c26008b"; // 5 langweil 27 - fixed

		t.models[mid].position = [-6.337880393338407, -5.387750078541261, 2.981515473794125];
		t.models[mid].target = [-0.05516190827583278, -0.1445960443415415, 0.11386374484248174];
		t.models[mid].speed = 1;		
		t.models[mid++].id	= "f2507500cbec4adbbf7da2679b910861"; // 6 hrnek
		
		t.models[mid].position = [-165.76377794817685, -9601.782478771529, 3724.236727321268];
		t.models[mid].target = [-165.76377794818927, -204.31351458303425, 670.8139661189762];
		t.models[mid].speed = 50;
		t.models[mid++].id	= "52c62d3a362d4c56a13490378dd6ee77"; // 7 klaster

		t.models[mid].position = [155.3709050124049, 160.20988294389673, 27.77554117802655];
		t.models[mid].target = [160.17121674021382, 29.12155016640908, 20.32527159214744];
		t.models[mid].speed = 1;
		t.models[mid++].id	= "b509e456e0e94e9ab15ac0117656a4de"; // 8 Earth - animated

		t.models[mid].position = [-165.76377794817685, -9601.782478771529, 3724.236727321268];
		t.models[mid].target = [-165.76377794818927, -204.31351458303425, 670.8139661189762];
		t.models[mid].speed = 50;		
		t.models[mid++].id	= "52c62d3a362d4c56a13490378dd6ee77"; // 9 free
		

		

		t.client = new Sketchfab( version, t.element );
		
		t.eye = [100,100,100];
		t.oeye = [0,0,0];
		t.target = [0,0,0];
		//t.speed = [2,5,10,20,40];
		t.speed = 1;
		t.maxspeed = 50;
		t.cspeed = 2;
		// t.mspeed = 150;
		t.mspeed = 500;
		t.started = false;
		t.lastx = 0;
		t.lasty = 0;
		t.annotationCount = 0;
		t.annotation = 0;
		t.ctx = null;
		t.annotationsVisible = false;
		t.initialized = false;

		// reset point
		// t.rEye = [0,0,0];
		// t.rTarget = [10,10,10];
		// currentlz selected model from preset 0-9
		t.currModel = 1;
		t.initialized = false;
                
		t.client.init(urlid, {
			success: function onSuccess( api ){
				t.api = api;
				t.started = true;
				t.api.start(function() {
					t.api.getCameraLookAt(function(err, camera){
						console.log(camera);
						t.eye[0] = camera.position[0];
						t.eye[1] = camera.position[1];
						t.eye[2] = camera.position[2];
						t.oeye[0] = t.eye[0];
						t.oeye[1] = t.eye[1];
						t.oeye[2] = t.eye[2];
					});
				});
			},error: function onError() {
				console.log( 'Viewer error' );
			},annotations_visible: 1,
			ui_controls: 0,
			ui_infos: 0,
			ui_stop: 0
		});

		t.initializeWidgets();
		createAnnotationElement();
	},
	
	initializeWidgets: function() {
		var t = this;

		this.controls.addButton({type: "default", position: 10, identifier: "Reset", label: "Reset"});
		this.controls.addButton({type: "default", position: 4, identifier: "Hide", label: "Annot"});
		this.controls.addButton({type: "next", position: 7, identifier: "Next"});
		this.controls.addButton({type: "prev", position: 1, identifier: "Prev"});
		
		this.controls.addTextInput({value: "", label: "ID:", identifier: "Load"});		
		
		this.controls.addSlider({
			identifier: "Speed",
			minimum: 1,
			maximum: t.maxspeed,
			increments: 1,
			property: "t.speed",
			label: "Speed"
			// labelFormatFunction: function(value, end) {
			// 	return ((value < 10) ? "0" : "") + value + "/" + end;
			// }
		});
		this.controls.finishedAddingControls();
	},

	load: function(state, date) {
	},
	
	draw: function(date) {
        var t = this;
        if (t.started && (t.eye[0] != t.oeye[0] || t.eye[1] != t.oeye[1] || t.eye[2] != t.oeye[2])) {
                t.api.lookat(t.eye, t.target, 0.3);
                t.oeye[0] = t.eye[0];
                t.oeye[1] = t.eye[1];
                t.oeye[2] = t.eye[2];
        }
	},
	
	resize: function(date) {
		var t = this;
		t.element.width = t.element.clientWidth;
		t.element.height = t.element.clientHeight;
		t.refresh(date);
	},
	
	event: function(eventType, position, user_id, data, date) {
		var t = this;
		/*
		 * Move functions
		 */
		function orbitLook(cr, cphi, comega) {
			if (!t.initialized){
				initCamera();
			}else{
				var x = t.eye[0] - t.target[0];
				var y = t.eye[1] - t.target[1];
				var z = t.eye[2] - t.target[2];
				var r = Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
				var phi = Math.atan2(y,x);
	            var omega = Math.acos(z/r);
	            var nr = r + cr;
	            var nphi = phi + cphi;
	            var nomega = omega + comega;
	            t.eye[0] = nr*Math.sin(nomega)*Math.cos(nphi) + t.target[0];
	            t.eye[1] = nr*Math.sin(nomega)*Math.sin(nphi) + t.target[1];
	            t.eye[2] = nr*Math.cos(nomega) + t.target[2];
	            //console.log("Move on "+x+"x"+y+": computed [r,phi,omega] = ["+r+","+phi+","+omega+"] and new [r,phi,omega] = ["+nr+","+nphi+","+nomega+"]");
	            //console.log(t.target[0], t.target[1], t.target[2]);
	        }
		}
		function walkLook(dist) {
			if (!t.initialized){
				initCamera();
			}else{
				var dx = t.eye[0] - t.target[0];
				var dy = t.eye[1] - t.target[1];
				var dz = t.eye[2] - t.target[2];
				size = Math.sqrt(dx*dx + dy*dy + dz*dz);
				dx = (dx/size)*dist;
				dy = (dy/size)*dist;
				dz = (dz/size)*dist;
				t.eye[0] -= dx;
				t.eye[1] -= dy;
				t.eye[2] -= dz;
				t.target[0] -= dx;
				t.target[1] -= dy;
				t.target[2] -= dz;
			}
		}
		function moveLook(cr, cphi, comega) {
			if (!t.initialized){
				initCamera();
			}else{
				var x = t.eye[0] - t.target[0];
				var y = t.eye[1] - t.target[1];
				var z = t.eye[2] - t.target[2];
				var r = Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2));
				var phi = Math.atan2(y,x);
	            var omega = Math.acos(z/r);
	            var nr = r + cr;
	            var nphi = phi + cphi;
	            var nomega = omega + comega;
	            nx = nr*Math.sin(nomega)*Math.cos(nphi);
	            ny = nr*Math.sin(nomega)*Math.sin(nphi);
	            nz = nr*Math.cos(nomega);
				var d = -(x*t.eye[0]+y*t.eye[1]+z*t.eye[2]);
			    var dt = -((x*t.target[0] + y*t.target[1] + z*t.target[2] + d)/(x*x+y*y+z*z));
			    neye = [];
			    neye[0] = t.target[0] + nx*dt;
			    neye[1] = t.target[1] + ny*dt;
			    neye[2] = t.target[2] + nz*dt;
			    
			    var dx = t.eye[0] - neye[0];
				var dy = t.eye[1] - neye[1];
				var dz = t.eye[2] - neye[2];
				size = Math.sqrt(dx*dx + dy*dy + dz*dz);
				dist = Math.sqrt(x*x + y*y + z*z)/50;
				if (size == 0) {
					console.log("moveLook, change size is: "+size+", not changing anything");
					return;
				}
				console.log("moveLook, change size is: "+size);
				dx = (dx/size)*dist;
				dy = (dy/size)*dist;
				dz = (dz/size)*dist;
				t.eye[0] -= dx;
				t.eye[1] -= dy;
				t.eye[2] -= dz;
				t.target[0] -= dx;
				t.target[1] -= dy;
				t.target[2] -= dz;
				// console.log("moveLook, new position eye: ["+t.eye[0]+","+t.eye[1]+","+t.eye[2]+"] and target: ["+t.target[0]+","+t.target[1]+","+t.target[2]+"]");
			}
		}

		function loadSource(urlid){
			t.initialized = false;
			clearAnnotationWindow();
			t.annotationCount = 0;
			t.client.init(urlid, {
				success: function onSuccess( api ){
					t.api = api;
					t.started = true;
					t.api.start(function() {
					t.api.getCameraLookAt(function(err, camera){
						console.log(camera);
						t.eye[0] = camera.position[0];
						t.eye[1] = camera.position[1];
						t.eye[2] = camera.position[2];
						t.oeye[0] = t.eye[0];
						t.oeye[1] = t.eye[1];
						t.oeye[2] = t.eye[2];
						t.rEye = camera.position;
						t.rTarget = camera.target;
					});
				});
					t.annotation = 0;
				},error: function onError() {
					console.log( 'Viewer error' );
				}
			});
		}

		function loadAnnontations(){
			t.annotation = 0;
			t.api.getAnnotationList( function( err, annotations ) {
    			console.log( annotations );
    			setAnnotations(err, annotations);
			});
		}

		function setAnnotations(err, annotations){
			t.annotationCount = annotations.length;
			t.annotations = annotations;
			console.log(t.annotations);
			console.log("annotations number " + t.annotationCount);

		}

		function hideAnnotationWindow(){
			t.annotationsVisible = false;
			t.canvas.style.visibility = "hidden";
		}

		function showAnnotationWindow(){
			t.annotationsVisible = true;
			t.canvas.style.visibility = "visible"
		}


		function toggleAnnotations(){
			// toggle annotations window visibility on/off
			if(t.annotationsVisible){
				hideAnnotationWindow();
			}else{
				showAnnotationWindow();
			}			
		}

		function clearAnnotationWindow(){			
			t.ctx.clearRect(0, 0, t.canvas.width, t.canvas.height);
			t.ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
			t.ctx.fillRect(0,0, t.canvas.width, t.canvas.height);
			hideAnnotationWindow();
		}

		function getLines(ctx, text, maxWidth) {
		    var words = text.split(" ");
		    var lines = [];
		    var currentLine = words[0];

		    for (var i = 1; i < words.length; i++) {
		        var word = words[i];
		        var width = ctx.measureText(currentLine + " " + word).width;
		        if (width < maxWidth) {
		            currentLine += " " + word;
		        } else {
		            lines.push(currentLine);
		            currentLine = word;
		        }
		    }
		    lines.push(currentLine);
		    return lines;
		}

		function displayAnnotation(){
			var fontSize = 70;
			var ySpace = fontSize;
			t.ctx.clearRect(0, 0, t.canvas.width, t.canvas.height);
			t.ctx.fillStyle = "rgba(0, 0, 0, 0.5)";
			t.ctx.fillRect(0,0, t.canvas.width, t.canvas.height);
			t.ctx.fillStyle = "#DDDDDD"
			t.ctx.font = "80px Arial";
			t.ctx.fillText("<< " + (t.annotation+1) + "/" + t.annotationCount + " >>" ,10,ySpace);
			ySpace += fontSize;
			t.ctx.fillText(t.annotations[t.annotation].name ,10, ySpace);
			
			var rows = getLines(t.ctx, t.annotations[t.annotation].content, t.canvas.width-10);
			var i;
			for (i = 0; i < rows.length; i++) {
				ySpace+=fontSize;
				t.ctx.fillText(rows[i], 10, ySpace);
			}			
		}

		function nextAnnotation(direction){
			var delta;
			if (direction=="right"){
				delta = 1;
			}else{
				delta = -1;
			}

			if(t.annotationCount == 0){
				loadAnnontations();
			}
			else if(t.annotationCount>0){
				if (delta < 0){
					t.annotation = (t.annotation+delta+t.annotationCount)%t.annotationCount;
				}else{
					t.annotation = (t.annotation+delta)%t.annotationCount;
				}
				
				t.eye[0] = t.annotations[t.annotation].eye[0];
				t.eye[1] = t.annotations[t.annotation].eye[1];
				t.eye[2] = t.annotations[t.annotation].eye[2];
				t.target[0] = t.annotations[t.annotation].target[0];
				t.target[1] = t.annotations[t.annotation].target[1];
				t.target[2] = t.annotations[t.annotation].target[2];
				console.log("going to annotation number " + t.annotation);
				displayAnnotation();
			}
		}

		function resetCamera(){
			// resets camera to initial view
			console.log("currModel = " + t.currModel);
			t.eye[0] = t.models[t.currModel].position[0];
			t.eye[1] = t.models[t.currModel].position[1];
			t.eye[2] = t.models[t.currModel].position[2];

			t.target[0] = t.models[t.currModel].target[0];
			t.target[1] = t.models[t.currModel].target[1];
			t.target[2] = t.models[t.currModel].target[2];

			// t.api.lookat(t.eye, t.target, 0.3);
			// hard reset if model is desynchronized
			// t.eye[0] = 10;
			// t.eye[1] = 10;
			// t.eye[2] = 10;

			// t.target[0] = 0;
			// t.target[1] = 0;
			// t.target[2] = 0;

			t.api.seekTo( 0.0 );
		}

		function resetInitPosition(){
			// var t = this;
			// t.api.getCameraLookAt(function(err, camera){
			// 			console.log(camera);
						
			// });
			// console.log("resetInitPosition end");
		}


		function initCamera(){
			t.api.getCameraLookAt(function(err, camera){
				t.eye[0] = camera.position[0];
				t.eye[1] = camera.position[1];
				t.eye[2] = camera.position[2];
				t.oeye[0] = t.eye[0];
				t.oeye[1] = t.eye[1];
				t.oeye[2] = t.eye[2];
				t.target[0] = camera.target[0];
				t.target[1] = camera.target[1];
				t.target[2] = camera.target[2];
				// t.rEye = camera.position;
				// t.rTarget = camera.target;
			});
			t.initialized = true;
		}

		function test(){
			t.api.getCameraLookAt(function(err, camera){
				console.log(camera.position);
				console.log(camera.target);
				// t.eye[0] = camera.position[0];
				// t.eye[1] = camera.position[1];
				// t.eye[2] = camera.position[2];
				// t.oeye[0] = t.eye[0];
				// t.oeye[1] = t.eye[1];
				// t.oeye[2] = t.eye[2];
				// t.rEye = camera.position;
				// t.rTarget = camera.target;
			});
			//console.log(t.target);

		}
		
		/*
		 * Mouse actions
		 */
		if (eventType === "pointerPress" && data.button === "left") {
			t.dragging = true;
			//console.log("Click on "+position.x+"x"+position.y);
			t.lastx = position.x;
			t.lasty = position.y;
		} else if (eventType === "pointerRelease" && data.button === "left") {
			t.dragging = false;
			//console.log("Relase on "+position.x+"x"+position.y);
		} else if (eventType === "pointerMove" && t.dragging && !t.moving) {
			cphi = (t.lastx-position.x)/t.mspeed;
			comega = (t.lasty-position.y)/t.mspeed;
			orbitLook(0, cphi, comega);
			t.lastx = position.x;
            t.lasty = position.y;
		} else if (eventType === "pointerMove" && t.dragging && t.moving) {
			cphi = (t.lastx-position.x)/t.mspeed;
			comega = (t.lasty-position.y)/t.mspeed;
			moveLook(0, cphi, comega);
			t.lastx = position.x;
            t.lasty = position.y;
		} else if (eventType === "pointerScroll") {
			orbitLook(data.wheelDelta/((t.maxspeed+1) - t.speed), 0, 0);
		}
		
		/*
		 * Keyboard actions
		 */
		if (eventType == "specialKey" && data.code == 37 && data.state == "down") {
			// left
			// go to previous annotation
			nextAnnotation("left");
		}
		else if (eventType == "specialKey" && data.code == 38 && data.state == "down") {
			// up - temporarily disabled
			// walkLook(t.speed);
		}
		else if (eventType == "specialKey" && data.code == 39 && data.state == "down") {
			// right
			// go to next annotation
			nextAnnotation("right");
		}
		else if (eventType == "specialKey" && data.code == 40 && data.state == "down") {
			// down - temporarily disabled
			// walkLook(-t.speed);
		}
		else if (eventType == "specialKey" && data.code == 33 && data.state == "down") {
			// pageup
		}
		else if (eventType == "specialKey" && data.code == 34 && data.state == "down") {
			// pagedown
		}
		else if (eventType == "specialKey" && data.code == 72 && data.state == "down") {
			// h
			toggleAnnotations();
		}
		else if (eventType == "specialKey" && data.code == 74 && data.state == "down") {
			// j
		}
		else if (eventType == "specialKey" && data.code == 75 && data.state == "down") {
			// k
			t.api.gotoAnnotation(0);
		}
		else if (eventType == "specialKey" && data.code == 84 && data.state == "down") {
			// t
			test();
		}
		else if (eventType == "specialKey" && data.code == 83 && data.state == "down") {
			// s
			// t.cspeed = (t.cspeed + 1) % 5;
			// console.log("camera speed " + t.cspeed);
		}
		else if (eventType == "specialKey" && data.code == 82 && data.state == "down") {
			// r
			// reset
			resetCamera();
		}
		else if (eventType == "specialKey" && data.code == 16 && data.state == "down") {
			// shift down
			t.moving = true;
		}
		else if (eventType == "specialKey" && data.code == 16 && data.state == "up") {
			// shift up
			t.moving = false;
		}
		else if (eventType == "specialKey" && data.code >= 96 && data.code <= 105 && data.state == "down") {
			// 0-9
			loadSource(t.models[data.code-96].id);
			t.currModel = data.code-96;
			t.speed = t.models[t.currModel].speed;
		}
		else if (eventType == "specialKey" && data.code >= 48 && data.code <= 57 && data.state == "down") {
			// 0-9
			loadSource(t.models[data.code-48].id);
			t.currModel = data.code-48;
			t.speed = t.models[t.currModel].speed;
		}
		else if (eventType == "specialKey" && data.code == 9 && data.state == "down") {
			// tab
		}
		else if (eventType == "specialKey" && data.state == "down") {
			console.log("Key pressed: " + data.code);
		}
		
		/*
		 * Widget actions
		 */
		if (eventType === "widgetEvent") {
			switch (data.identifier) {
				case "Load":
					loadSource(data.text);
					break;
				case "Speed":
					switch (data.action) {
						case "sliderLock":
							break;
						case "sliderUpdate":
							break;
						case "sliderRelease":
							break;
						default:
							console.log("No handler for: " + data.identifier + "->" + data.action);
							break;
					}
					break;
				case "Reset":
					resetCamera();
					break;
				case "Hide":
					toggleAnnotations();
					break;
				case "Next":
					nextAnnotation("right");
					break;
				case "Prev":
					nextAnnotation("left");
					break;	
				default:
					console.log("No handler for:", data.identifier);
			}
		}
		
		this.refresh(date);
	}
});


