# Sketchfab Viewer #

## Description ##

A SAGE2 application for visualizations of 3D models uploaded to the Sketchfab service (https://sketchfab.com). Implemented using Javascript and Sketchfab Viewer API. Runs on multiple PCs that drive a SAGE2 wall, synchronized by SAGE2 messages.

## Features ##

* orbit look and zoom
* adjustable zoom speed
* 3D animations support
* annotations support
* loading new images from sketchfab.com by typing model IDs
* up to 10 preset models for easy access

## Installation ##

* copy sketchfab_viewer directory to ./public/upload/apps/
* restart SAGE2 server

## Use ##

Mouse

* Left mouse button - orbit look
* Shift + Left mouse button - pan
* Scroll mouse - zoom

Hotkeys

* R - Reset (resets camera, object and animation to original state)
* H - Hide/Show annotations
* Right - Next annotation
* Left - Previous annotation
* 0-9 - Load preset models

Widget

* ID - load model by ID (e.g., a9b569b66ea74e709420718a7dc567e4 )
* Speed - adjusts zoom speed
* Annot - Hide/Show annotations

## Contact ##

Jiri Kubista, CESNET, Jiri.Kubista@cesnet.cz

![sketchfab_viewer.jpg](https://bitbucket.org/repo/B7eEkX/images/2831865891-sketchfab_viewer.jpg)